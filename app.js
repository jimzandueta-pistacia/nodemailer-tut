var express = require("express");
var port = process.env.PORT || 3000;
var app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set("view engine", "ejs");


var nodemailer = require("nodemailer");
var fs = require("fs");


app.get("/", function (req, res) {
    res.send("Hello");
})


app.get("/mailer", function (req, res) {
    res.render("mailer");
});
app.post("/mailer", function (req, res) {

    var data = {}
    data.from = "'Hello' <pistacia.captiveportal@gmail.com>";
    data.to = req.body.email;
    data.subject = "Email from " + req.body.name;
    data.html = "<h1>NEW MESSAGE!</h1><br><span> " + req.body.message + " </span>";

    console.log(data);

    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
            user: "pistacia.captiveportal@gmail.com",
            pass: ""
        }
    });

    let mailOptions = {
        from: data.from, // sender address
        to: data.to, // list of receivers
        subject: data.subject, // Subject line
        html: data.html // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            return console.log(err);
        }
        console.log("Message %s sent: %s", info.messageId, info.response);
        res.render("post_mailer");
    });

});


app.get("/signup", function (req, res) {
    res.render("signup");
});
app.post("/signup", function (req, res) {

    let rand = Math.random();
    let confirmation_data = {
        "email": req.body.email,
        "code": rand
    }
    fs.writeFileSync(req.body.email, JSON.stringify(confirmation_data));

    var data = {}
    data.from = "'Hello' <pistacia.captiveportal@gmail.com>";
    data.to = req.body.email;
    data.subject = "Confirm your email";
    data.html = "Click link to Confirm to confirm. <br> <a href='http://localhost:3000/confirm?" +
        "em=" + confirmation_data.email +
        "&code=" + confirmation_data.code + "'> Confirm! </a>";

    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true, // secure:true for port 465, secure:false for port 587
        auth: {
            user: "pistacia.captiveportal@gmail.com",
            pass: ""
        }
    });

    let mailOptions = {
        from: data.from, // sender address
        to: data.to, // list of receivers
        subject: data.subject, // Subject line
        html: data.html // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            return console.log(err);
        }
        console.log("Message %s sent: %s", info.messageId, info.response);
        res.render("post_signup");
    });
})


app.get("/confirm", function (req, res) {
    var raw_data = fs.readFileSync(req.query.em);
    var data = JSON.parse(raw_data);

    if (req.query.code == data.code) {
        res.send("confirmed!");
    } else {
        res.send("problem with confirmation");
    }
})

app.listen(port);